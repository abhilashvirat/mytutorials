//Declare a variable
const person = {
  firstName: "Harry",
  lastName: "Potter",
  dob: {
    day: 31,
    month: "July",
    year: 1980,
  },
  address: {
    primaryAddress: {
      street: "4 Private Drive, Little Whinging, Surrey",
      city: "London",
      zipcode: "12345",
    },
  },
};
console.clear();
console.log(person);

// Simple Destructure
const { firstName, lastName } = person;
console.log("firstName = ", firstName);
console.log("lastName = ", lastName);

//nested destructure
const {
    dob : { day, month, year},
 } = person;
 console.log("day = ", day);
 console.log("month = ", month);
 console.log("year = ", year);
 


//deep nested destructure
const {
   address: { primaryAddress : { city, street, zipcode }},
} = person;
console.log("city = ", city);
console.log("street = ", street);
console.log("zipcode = ", zipcode);
