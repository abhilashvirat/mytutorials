const person = {
    firstName: "Harry",
    lastName: "Potter",
    dob: {
      day: 31,
      month: "July",
      year: 1980,
    },
    address: {
      primaryAddress: {
        street: "4 Private Drive, Little Whinging, Surrey",
        city: "London",
        zipcode: "12345",
      },
    },
  };

  console.clear();

  //get only values from object
  console.log('Only values = ', Object.values(person))

  console.log('values as key val pair = ', Object.entries(person))