const person = {
    firstName: "Harry",
    lastName: "Potter"
  };

console.clear();  
console.log('person = ', person);


person.dob = "4th Sep 1992";
person.address = { city: 'Pune', zip: 123434}
console.log('person with dob = ', person);
// This code works as we are not really changing the refrance of object
// we are sipmly pushing the values into exisitng object

//but if try to change the refrance then we should get the error
person = {name: "Jack Sparrow"};
console.log('with ref change', person);
